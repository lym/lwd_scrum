# Scrum Board Data Map

# Notes
- A task board is commonly used in Scrum-style development to manage
  tasks in the current sprint.

- At the highest level we have *Sprints* with an end date but optional
  name and description.

- *Sprints* consist of tasks.

- Tasks are moved from the backlog into the set of tasks to be
  completed.

- Various states can be used to track the progress along the way, such
  as "in progress" or "in testing".

- The preceding tasks must be part of a task for it to be in a
  "Completed" state.

# Dependencies
- Postgresql - for permanent persistance

- tornado server - for websockets

# Usage
Clone this project to your local development box:

    $ cd [project directory]

    $ git clone git@bitbucket.org:lym/lwd_scrum.git

Create a virtual environment, to isolate your project, with:

    virtualenv [path to you prefered python version] env

Install `pip-tools` for dependency management

    $ pip install pip-tools

Download the project dependencies with *pip-tools*

    $ pip-compile requirements.in
    $ pip-sync requirements.txt

Create a database for the application with the necessary access rights.
For example if you use postgresql with _trust_ auth, you can create a
database with the following command:

    $ createdb <database name>

Apply the project migrations to your postgresql instance

    $ python manage.py migrate

Create an admin user by running:

    $ python manage.py createsuperuser

Start the main app server by the running the command:

    $ env/bin/python manage.py runserver`

Start the websockets server by running

    env/bin/python watercooler.py

Load up: `http://localhost:8000/#sprint/1`

Log in and start playing around.
